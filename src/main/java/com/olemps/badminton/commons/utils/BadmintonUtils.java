package com.olemps.badminton.commons.utils;

public final class BadmintonUtils {

    private static final String LICENSE_PATTERN = "[0-9]{8}";

    private BadmintonUtils() {
    }

    public static boolean isLicense(String str) {
        return str != null && str.matches(LICENSE_PATTERN);
    }
}
