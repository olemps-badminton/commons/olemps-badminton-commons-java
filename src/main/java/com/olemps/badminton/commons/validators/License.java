package com.olemps.badminton.commons.validators;

import javax.validation.Constraint;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = LicenseValidator.class)
@Target(FIELD)
@Retention(RUNTIME)
public @interface License {

    String message = "";

    Object[] groups = new Object[]{};

    Object[] payLoad = new Object[]{};
}
