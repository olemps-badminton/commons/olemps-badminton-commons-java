package com.olemps.badminton.commons.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static com.olemps.badminton.commons.utils.BadmintonUtils.isLicense;

public class LicenseValidator implements ConstraintValidator<License, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || isLicense(value);
    }
}
